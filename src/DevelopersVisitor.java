import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.repodriller.domain.Commit;
import org.repodriller.persistence.PersistenceMechanism;
import org.repodriller.scm.CommitVisitor;
import org.repodriller.scm.SCMRepository;

public class DevelopersVisitor implements CommitVisitor {

	@Override
	public void process(SCMRepository repo, Commit commit, PersistenceMechanism writer) {
		
		try {
			System.out.println("Commit: " + commit.getHash());
			String[] command = {"C:/Users/Marcos/workspace/searchEmail/run_maven.bat", repo.getPath().toString()};
			runScript(command);
			command[0] = "C:/Users/Marcos/workspace/searchEmail/generate_jar.bat";
			runScript(command);
			
			if (App.main(repo.getPath())){
				//Escreve email do desenvolvedor no arquivo
				BufferedWriter bf = new BufferedWriter(new FileWriter("C:/Users/Marcos/Desktop/a.txt", true));
				bf.append(commit.getAuthor().getEmail());
				bf.newLine();
				bf.flush();
				bf.close();
			}
			
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		} 
	}
	
	public static void runScript(String[] command) throws FileNotFoundException,
	UnsupportedEncodingException, IOException, InterruptedException {

		ProcessBuilder processBuilder = new ProcessBuilder(command);
		processBuilder.redirectErrorStream(true);
		Process process = processBuilder.start();
		process.waitFor();
	}

	@Override
	public String name() {
		return "developers";
	}
}