import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.repodriller.RepoDriller;
import org.repodriller.RepositoryMining;
import org.repodriller.Study;
import org.repodriller.filter.commit.OnlyModificationsWithFileTypes;
import org.repodriller.filter.range.Commits;
import org.repodriller.scm.GitRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


public class MyStudy implements Study {
	
	public static void main(String[] args) {
		new RepoDriller().start(new MyStudy());
	}
	
	@Override
	public void execute() {
		try {
			
			cloneRepositories();
			new RepositoryMining()
			.in(GitRepository.allProjectsIn("C:/Users/Marcos/Desktop/Nova pasta"))
			.through(Commits.all())
			.filters(new OnlyModificationsWithFileTypes(Arrays.asList(".java" )))
			.process(new DevelopersVisitor())
			.mine();
			
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	protected void cloneRepositories() throws FileNotFoundException, UnsupportedEncodingException, IOException, InterruptedException {
		try {
			Gson gson = new GsonBuilder().create();
			
			@SuppressWarnings("serial")
			List<Repo> repositories = gson.fromJson(getFile("repositories.json"), new TypeToken<List<Repo>>() {
			}.getType());

			for (Repo repo : repositories) {
				File path = new File("C:/Users/Marcos/Desktop/Nova pasta/" + repo.getPath());

				if (!path.exists()) {
					// FileUtils.deleteDirectory(repositories.get(uri));
					System.out.println("Cloning repository " + repo.getUrl());
					Git git = Git.cloneRepository().setURI(repo.getUrl()).setDirectory(path).call();
					git.close();
				}
				
			}
		} catch (InvalidRemoteException e) {
			e.printStackTrace();
		} catch (TransportException e) {
			e.printStackTrace();
		} catch (GitAPIException e) {
			e.printStackTrace();
		}
	}

	private String getFile(String fileName) {

		StringBuilder result = new StringBuilder("");

		File file = new File("C:/Users/Marcos/workspace/searchEmail/repositories.json");

		try (Scanner scanner = new Scanner(file)) {

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				result.append(line).append("\n");
			}

			scanner.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return result.toString();

	}

}
